# tf2_samples

The tutorial samples for tensorflow2

## 1. mlp.ipynb
The first naive mlp model trained on keras.datasets 

## 2. cnn.ipynb
The first naive cnn model trained on keras.datasets

## 3. cnn_tfdata.ipynb
The cnn model trained on images from the folder by tf.data